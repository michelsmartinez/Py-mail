import smtplib

smtp = smtplib.SMTP_SSL('smtp.gmail.com', 465) #verificar isto caso não for utilizar Gmail

smtp.login('email@gmail.com', 'senha') #alterar para seu email e senha (este é o "From")

de = 'email@gmail.com'
para = ['to@gmail.com'] #email que receberá o email
msg = """From: %s
To: %s
Subject: Testando e-mail

Email de teste - Michel Martinez.""" % (de, ', '.join(para))

smtp.sendmail(de, para, msg)

smtp.quit()
